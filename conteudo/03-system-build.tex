%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{System Build}\index{System Build}

This chapter presents an introduction to both the Linux kernel build system and
devicetree infrastructure. The former is responsible for generating the
appropriate kernel modules and images according to stored configuration and
compilation flags. The latter provides a way of telling the kernel what the
hardware layout is. The basic knowledge of these is necessary to understand
further changes a driver implementation does besides its core file.

\section{The Kernel Build System (kbuild)}\index{The Kernel Build System}

The Kernel Build System (kbuild) is a framework based on make, flex, and other
GNU tools that allows a highly modular and customizable compilation process for
the Linux kernel. Kconfig and Kbuild Makefiles implement most of the kbuild
features. The framework grants flexibility by conditional compilation based on
configuration options.

\subsection{Configuration Options}

By default, kbuild Makefiles use the configuration options stored in the
\textit{.config} file under the kernel root directory. These options hold values
for the configuration symbols associated with kernel resources (drivers, tools,
features, etc.). Changes in the configuration options reflect on what kbuild
generates. Moreover, the configuration options are orderly sensitive. A disabled
option may limit the visibility of dependent entries. Thus, directly editing the
\textit{.config} file requires caution. Nevertheless, to easy testing with
configuration files, an alternative \textit{.config} may be set. Export the path
to the \textit{KCONFIG\_CONFIG} variable to use a custom configuration source.
For instance:

\begin{lstlisting}[language=Bash, style=wider]
export KCONFIG_CONFIG=.my_config
\end{lstlisting}

Alternatively, we may set the \textit{.config} file just when invoking make.

\begin{lstlisting}[language=Bash, style=wider]
make KCONFIG_CONFIG=.my_config
\end{lstlisting}

This option is not much used, though. Many of the thousand values hold by
configuration files are common to multiple applications. A cleaner way of
storing custom values for configuration symbols is to use \texttt{defconfig}.
(We will look at defconfig in Section 3.1.3). 

\subsection{Configuration Symbols}

The \textit{Kconfig} files define the configuration symbols associated with the
kernel resources. As a general rule, a \textit{Kconfig} file should only declare
symbols for resources under the same directory. Nearly all directories inside
the kernel source tree have a \textit{Kconfig} file. Top \textit{Kconfig} files
include (source) \textit{Kconfig} files from subdirectories thus, creating a
tree of configuration symbols. To define a configuration symbol for the AD7292
driver, the following entry was added to the \textit{Kconfig} file at
\textit{drivers/iio/adc/}.

\begin{verbatim}
config AD7292
	tristate "Analog Devices AD7292 ADC driver"
	depends on SPI
	help
	  Say yes here to build support for Analog Devices AD7292
	  8 Channel ADC with temperature sensor.

	  To compile this driver as a module, choose M here: the
	  module will be called ad7292.
\end{verbatim}

The \textit{config} keyword defines a new configuration symbol, which in turn is
presented as a configuration entry in the \textit{.config} file, within tools
like \textit{menuconfig}, \textit{nconfig}, or during the compilation process.
In particular, the AD7292 configuration symbol has the following attributes:

\begin{description}
  \item [tristate:] the type for the configuration option. It declares that this
  symbol stands for something that may be compiled as a module (m), built-in
  compiled (y) (i.e., included in the kernel image), or not compiled at all (n).
  The type definition also accepts an optional input prompt to set the option
  name that kernel configuration tools display.
  \item [depends on:] list of dependency symbols. If its dependencies are not
  satisfied, this symbol may become non-visible during configuration or
  compilation time. As an experiment, try to disable SPI\footnote{The Serial
  Peripheral Interface (SPI) is a communication protocol commonly used to
  exchange data between computers and small peripherals.} support at Device
  Drivers. The AD7292 will no longer be listed at \textit{Device Drivers
  $\rightarrow$ Industrial I/O support $\rightarrow$ Analog to digital
  converters}.
  \item [help:] defines a help text to be displayed as auxiliary info.
\end{description}

Additionally, if a configuration option has no value, the default value is used.
If no default is available, the user will be prompted to assign it a value. Keep
in mind that a \textbf{configuration option} stores the value assigned to a
\textbf{configuration symbol}. Configuration options have the form
\textit{CONFIG\_<symbol>}. For instance, \textit{CONFIG\_AD7292} stores the
value for the AD7292 configuration symbol.

The kernel source code comes with no \textit{.config} file, so one has to be
created. Though the compilation process can automatically generate configuration
files, it will ask for many configuration values. If some incompatible values
are assigned, the resulting image might not work on the desired machine.
Programs such as \textit{menuconfig} and \textit{nconfig} present the options
available in a menu like interface. The user may use them to find out
information about each option as well as to assign values to them. The program
then generates a configuration file with the desired values. Non assigned
symbols get default values or are prompted for in the compilation process.
Alternatively, one can use common values for the platform of interest. These
platform-specific values are stored in \textit{defconfig} files.

\subsection{Defconfig}

The purpose of \textit{defconfig} files is to store only specific non-default
values for compilation symbols. For instance, one can find \textit{defconfig}
files for the ARM architecture under \textit{arch/arm/configs/}. The files
\textit{bcm2709\_defconfig}, \textit{bcm2835\_defconfig}, and
\textit{bcmrpi\_defconfig} store the configuration values commonly used for
Raspberry Pi boards. To add a custom configuration value for the AD7292 symbol,
add the following line to a \textit{defconfig} file.

\begin{verbatim}
CONFIG_AD7292=y
\end{verbatim}

The configuration stored at a \textit{defconfig} file may be applied to
\textit{.config} using its name as a make target. For instance, to load
configuration options from \textit{bcm2709\_defconfig}, one may invoke:

\begin{lstlisting}[language=Bash, style=wider]
make bcm2709_defconfig
\end{lstlisting}

Use the \texttt{savedefconfig} target to create a \textit{defconfig} file from
\textit{.config}.

\begin{lstlisting}[language=Bash, style=wider]
make savedefconfig
\end{lstlisting}

So far, we have seen how to define a configuration symbol and assign it a value.
However, we still need to understand how to make kbuild compile a driver source
file according to a configuration option. Some knowledge of kbuild makefiles
will help us to do that.

\subsection{Kbuild Makefiles}

The main goal of the kbuild Makefiles is to produce the vmlinux (kernel image)
and modules. It builds them by recursively descending into subdirectories of the
kernel source tree~\cite{Kbuild:KbuildMakfiles}. Akin to \textit{Kconfig} files,
kbuild Makefiles are also present in most kernel directories, often working with
the values assigned for the symbols defined by the former. According to Javier
Canillas:

\begin{quote}
The whole build is done recursively — a top Makefile descends into its
subdirectories and executes each subdirectory's Makefile to generate the binary
objects for the files in that directory. Then, these objects are used to
generate the modules and the Linux kernel
image~\cite{Kbuild:LinuxJournal,Kbuild:delivery}.
\end{quote}

Makefiles in subdirectories should only modify files in their own directory.
Thus, we include driver object files in the list of kbuild compilation goals
inside the nearby makefile. For instance, the AD7292 driver has its entry inside
\textit{drivers/iio/adc/Makefile}:

\begin{lstlisting}[language=Make, style=wider]
obj-$(CONFIG_AD7292) += ad7292.o
\end{lstlisting}

To summarize the procedure of adding a feature to the Linux kernel, Canillas
points out three main steps:

\begin{quote}
- Put the source file(s) in a place that makes sense, such as
drivers/net/wireless for Wi-Fi devices or fs for a new filesystem.

- Update the Kconfig for the subdirectory (or subdirectories) where you put
the files with config symbols that allow you to choose to include the feature.

- Update the Makefile for the subdirectory where you put the files, so the build
system can compile your code
conditionally~\cite{Kbuild:LinuxJournal,Kbuild:delivery}.
\end{quote}

Symbols, options, and makefiles set up, we may now compile the kernel to test
our changes.

\section{Kernel Compilation}\index{Kernel Compilation}

%The Linux kernel may be compiled calling GNU make at the root of the source
%directory.

To compile the Linux kernel, call GNU make at the root of the source dircetory.

\begin{lstlisting}[language=Bash, style=wider]
make
\end{lstlisting}

The default target (\_all) will build the bare kernel image (vmlinux), all of
the modules, and other architecture-specific artifacts. By default, the top
Makefile sets \texttt{\$(ARCH)} to be the same as the host system
architecture~\cite{Kbuild:KbuildVariables}. However, this may not reflect the
architecture of the machine one want to run the kernel. Since different computer
architectures have distinct instruction sets, code compiled in one computer may
not work on another of incompatible design. Nevertheless, it is often desired to
use a host machine to generate binaries compatible with a target machine of
different architecture. Doing so is called cross-compilation.

% talk about other targets (modules, modules\_install, clean, help)?

\subsection{Kernel Cross Compilation}\index{Kernel Cross Compilation}

Cross-compilation is assisted by kbuild mainly through the \texttt{ARCH} and
\texttt{CROSS\_COMPILE} variables. \texttt{ARCH} set the target architecture,
which is often the same name as the architecture directory under the
\textit{arch} directory~\cite{Kbuild:KbuildARCH}. \texttt{CROSS\_COMPILE}
specify part of the cross compiler filename or the path to it. Both variables
can be set in the shell environment or during the invocation of make. To compile
the Linux kernel for ARM do:

\begin{lstlisting}[language=Bash, style=wider]
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make zImage modules dtbs -j 4
\end{lstlisting}%\todo{tirar o número que aparece no início do comando. Isso pode confundir o leitor(a).}

%Custom cross compilers such as Linaro's GCC~\cite{Linaro:GCC} may also be used.

The compilation process might take some time (around half an hour on an x86
quad-core machine). The number of CPU cores used for the compilation is set by
the \texttt{-j} flag. The \texttt{dtbs} target makes kbuild compile the
devicetree files at \textit{arch/arm/boot/dts} into devicetree blobs that the
boot process will pass to kernel. With the \texttt{modules} target, kbuild will
build code marked as module (m). The \texttt{zImage} refers to ARM specific
image format. Different from the default \textit{vmlinux} image, the
\textit{zImage} is a compressed kernel image. If everything goes well, the last
output lines should look like:

\begin{verbatim}
  LD      arch/arm/boot/compressed/vmlinux
  OBJCOPY arch/arm/boot/zImage
  Kernel: arch/arm/boot/zImage is ready
\end{verbatim}

The compilation process will create (or overwrite) the following files:

%\begin{description}
%  \item [\textit{arch/arm/boot/zImage}:] the kernel image
%  \item [\textit{modules.order}:]
%  \item [\textit{modules.builtin}:] list of the modules built into the kernel.
%  \item [\textit{modules.builtin.modinfo}:] information about builtin modules.
%  \item [\textit{arch/arm/boot/dts/*.dtb}:] device tree blob files. These files
%  describe board hardware in a compact way.
%  \item [\textit{arch/arm/boot/dts/overlays/*.dtbo}:] device tree overlay blobs.
%  These are used to extend device tree hardware description.
%\end{description}
%https://www.kernel.org/doc/html/latest/kbuild/kbuild.html

\begin{itemize}
  \item \textit{arch/arm/boot/zImage}
  \item \textit{modules.order}
  \item \textit{modules.builtin}
  \item \textit{modules.builtin.modinfo}
  \item \textit{arch/arm/boot/dts/*.dtb}
  \item \textit{arch/arm/boot/dts/overlays/*.dtbo}
\end{itemize}

Many devices integrate systems that run on ARM architecture computers. Though,
hardware discovery on the ARM architecture is not as straight forward as in the
x86 architecture. In most x86 machines, the system kernel can discover what
equipment is available by looking at device tables provided by ACPI or UEFI
compliant firmware~\cite{ULK3-ACPI:OReilly}. Most ARM-based devices are not like
that, though.There is no implementation of the ACPI standard nor any means of
discovering which hardware is attached to the
system~\cite{DT-lang:LWN,DT-schema:LWN}. Moreover, ACPI raises many problems
regarding performance and security~\cite{ACPI-concerns:LWN}. Due to this, the
Linux kernel development has been adopting an alternative way to recognize
hardware layout, the Devicetree.

% Devicetree {
\section{Devicetree}

The \textbf{Device Tree (DT)} is a specification on how to describe the hardware
on a given system. It is concise, yet very expressive. A devicetree source (DTS)
file is a data structure made out of nodes that hold information about each
hardware component in a single board computer (SBC) or system on a chip (SoC).
Nodes may have property definitions and child node definitions. Properties can
hold 32-bit integer cells, strings, hexadecimal bytestrings, references to other
nodes, or can be left empty. To simplify node referencing and make the structure
more readable, developers may use labels to create aliases to nodes. DTS may
also include definitions from devicetree include files (DTSI), which in turn may
incorporate definitions from other DTSI as well~\cite{DTspec:DT}.


The Device Tree Compiler (DTC) turns DTS into Device Tree Binary (DTB) files. A
DTB is a flattened binary blob that encodes devicetree data within a compact
pointerless structure. A DTB file is passed to the system kernel by the
bootloader or wrapped up with the kernel image to support booting on legacy
non-DT aware firmware~\cite{DTspec:DT,Linux-and-DT:Kernel,DTCM:KernelGit}.
Early in the boot, the kernel parses the DTB to identify the machine and execute
any specific platform setup. At some later point in the kernel initialization,
the list of device nodes is obtained from the DTB and used to populate the Linux
Device Model with data about the platform~\cite{Linux-and-DT:Kernel}.

%wrapped up with the kernel image or passed to the kernel
%by the boot loader.
%
%scans
%
%then uses the device nodes to populate the system
%dynamically~\cite{Linux-and-DT:Kernel}.
%
%After the machine is set up, the kernel initialization proceeds normally.
%binary blob
%that is wrapped up with the kernel or passed to the system it by the boot loader.
%Documentation/devicetree/usage-model.txt
%https://github.com/torvalds/linux/blob/master/Documentation/devicetree/usage-model.txt
%Grant Likely
%
%A flattened device-tree block with header in one binary blob.
%https://git.kernel.org/pub/scm/utils/dtc/dtc.git/plain/Documentation/manual.txt?id=HEAD
%
%DTB files are included with the kernel or passed to it at boot time
%wrapped up with the kernel image to support booting existing non-DT aware firmware.

%Sensor devices may be described as devicetree nodes. These nodes can be
%interpreted by the kernel so that it gets to know the system hardware.

A \textbf{device node} is some devicetree node that describes a sensor device.
Device nodes have a \texttt{compatible} property that holds one or more strings
specifying the device model compatibility, from most specific to most general.
The order is important to allow devices to indicate their compatibility with
families of device drivers.  The recommend format is
\textit{``manufacturer,model''}, where \textit{manufacturer} stands for the
manufacturer name (or codename), and \textit{model} stands for the device model
name or number. For instance:

\begin{verbatim}
compatible = "samsung,exynos3250-adc", "samsung,exynos-adc-v2";
\end{verbatim}

For a device node with such a \textit{compatible} list, the first match attempt
will be against a device driver compatible with \textit{samsung,exynos3250-adc}.
If no such a driver is found, then there will be a match try against a driver
compatible with \textit{samsung,exynos-adc-v2}.

%see devicetree\_mysteries
%#include Vs /include/
%
%all Linux device drivers should provide documentation on how to describe the
%associated hardware in a devicetree.

Device node documentation followed a format derived from the Open Firmware (OF)
standard that was used mostly in PowerPC and SPARC platforms.%\todo{citar Documentation/devicetree/usage-model.txt}. 
These plain text docs were not very
restrictive about the structure of device nodes so, it was easy to make mistakes
when writing them. Developers are trying to avoid further misconceptions by
working in a set of validation tools known as dt-schema.

\textbf{Devicetree schema} files (also known as \textbf{DT bindings}) describe
how should be the format of data in a DTS. Devicetree schema is written in YAML
format and validated by dt-schema to restrict the schema structure to a subset
of the DT specification proper for describing device nodes. The purpose of
writing devicetree schemas is to provide a way to check whether a device node
inside a DTS file is correct and to provide documentation about device binding.
A DTS may contain nodes with different properties; therefore, many schema files
may be used to validate a single DTS~\cite{DT-schema:dt-schema}. The most common
fields of devicetree schema are:

%Documentation on how to write devicetree schemas can be found at
%Documentation/devicetree/writing-schema.rst

\begin{description}
  \item [\$id:] an UID (unique identifier) for the dt-binding.
  \item [\$schema:] the meta-schema that will be used to validate this binding.
  \item [title:] documentation for the documentation.
  \item [maintainers:] enum of maintainers.
  \item [description:] mandatory property specifying what device binding is being
  documented and where to find the device datasheet.
  \item [properties:] list of custom properties for the device node. May include
  several subproperties describing what a device node may contain and what
  values each subproperty is expected to have.
  \item [required:] enum of properties that must be present in a device node that
  describes such a device.
  \item [examples:] enum of examples showing how such a device node would appear
  in a DTS file. It is good practice to provide examples showing the usage of
  all the documented properties.
\end{description}

A simplified devicetree schema for AD7292 would look like Figure
\ref{fig:ad7292-dt-schema-sample}.

\estiloYAML
\begin{figure}[ht!]
%\centering
\begin{minipage}{\linewidth}
\begin{lstlisting}
$id: http://devicetree.org/schemas/iio/adc/adi,ad7292.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Analog Devices AD7292 10-Bit Monitor and Control System

maintainers:
  - Marcelo Schmitt <marcelo.schmitt1@gmail.com>

description: |
  Analog Devices AD7292 10-Bit Monitor and Control System with ADC, DACs,
  Temperature Sensor, and GPIOs

  Specifications about the part can be found at:
    https://www.analog.com/media/en/technical-documentation/data-sheets/ad7292.pdf

properties:
  compatible:
    enum:
      - adi,ad7292

  reg:
    maxItems: 1

  vref-supply:
    description: |
      The regulator supply for ADC and DAC reference voltage.

  spi-cpha: true

required:
  - compatible
  - reg
  - spi-cpha

examples:
  - |
    spi {
      #address-cells = <1>;
      #size-cells = <0>;

      ad7292: adc@0 {
        compatible = "adi,ad7292";
        reg = <0>;
        spi-max-frequency = <25000000>;
        vref-supply = <&adc_vref>;
        spi-cpha;
      };
    };

\end{lstlisting}
\end{minipage}
\caption{A simplified devicetree schema for AD7292.}
\label{fig:ad7292-dt-schema-sample}
\end{figure}

The AD7292 schema uses additional properties to describe an AD7292 device. Let's
see what some of them mean.

\begin{description}
  \item [compatible:] string to match with supporting device drivers.

  \item [reg:] the location of the device resources within the parent node
  address space. The specification for \textit{reg} properties was designed to
  allow the description of (\textit{address}, \textit{length}) pair values that
  point out the addresses, and how many bytes from each address, are part of the
  specified device resources. For SPI buses, it is only needed to specify the
  chip select (CP) lane on which the device is connected. This can be done with
  a single 32-bit unsigned integer thus, \textit{\#address-cells} is set to 1.
  Moreover, the SPI protocol does not specify any means of a slave device
  exposing more than its address to the master device. No memory range can be
  exposed directly on the bus. Thus, \textit{\#size-cells} for SPI buses should
  always be zero.

  \item [spi-max-frequency:] maximum SPI operating frequency.

  \item [vref-supply:] AD7292 may be supplied with an external voltage
  reference. When present, this property value points to the external
  reference. Otherwise, an internal voltage reference is used.

  \item [spi-cpha:] whether the chip
  requires shifted clock phase.%\todo{what is the 'default' IO clock edge?} 

  \item [\#address-cells:] how many 32-bit cells are needed to express the
  children node addresses. In the above example, the \textit{\#address-cells}
  property at the spi node specifies that the \textit{reg} field of its
  children (such as the ad7292 node) will have only one 32-bit value for
  addressing.

  \item [\#size-cells:] how many 32-bit cells are needed to express the children
  node address sizes. In the above example, the \textit{\#size-cells} property
  at the spi node specifies that the \textit{reg} field of its children nodes
  will not indicate any address range.
\end{description}

To insert an Analog Devices AD7292 control system into a devicetree, add it as a
child node of an SPI bus. A hypothetical simplified DTS that includes an AD7292
device would look like Figure \ref{fig:dts-example}.

\estiloYAML
\begin{figure}[ht!]
%\centering
\begin{minipage}{\linewidth}
\begin{lstlisting}
/ {
    compatible = "hlusp,tommy", "poli,caninos";

    cpus: cpus {
        <cpu nodes>
    }

    memory@0 {
        <memory properties>
    };

    spi: spi@7e204000 {
        compatible = "poli,caninos-spi";
        reg = <0x7e204000 0x200>;
        #address-cells = <1>;
        #size-cells = <0>;

        ad7292: adc@0 {
            compatible = "adi,ad7292";
            reg = <0>;
            spi-max-frequency = <25000000>;
            vref-supply = <&adc_vref>;
            spi-cpha;
        };
    };
};
\end{lstlisting}
\end{minipage}
\caption{An example of how an AD7292 device node would be set into a DTS file.}
\label{fig:dts-example}
\end{figure}

Now that we understand the basics of how to provide drivers and devices to the
system, we may focus on understanding how a device driver implementation works
on Linux.
%\todo{tenho nas minhas anotações que no final de cada capítulo devo adicionar roadmap. Isso seria o equivalente a um gancho para o próximo capítulo?}
% } Devicetree


%PAULO: continuar o trecho acima e fazer um gancho para o próximo capítulo.
