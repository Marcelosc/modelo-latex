# Linux Device Driver Development - A participant-observer case study

## Institute of Mathematics and Statistics of University of São Paulo

### Student: Marcelo Schmitt 

### Supervisor: Paulo RM Meirelles
### Co-Supervisor: Fabio Kon

This work is about developing a device driver for the Linux kernel and going
through the process of submitting it to Linus Torvalds' kernel repository.

## License

This monograph is based on IME/USP LaTeX Template. The licenses for the files
derived from the IME/USP template are acknowledged in the repository of that
project. All other content derived from Marcelo Schmitt's graduate work is
available under the [Creative Commons Attribution International License, v4.0
(CC-BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
